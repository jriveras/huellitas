<?php
require_once 'persistencia/conexion.php';
require_once 'persistencia/tipoProductoDAO.php';
class tipoProducto{
    private $id;
    private $nombre;
    private $Conexion;
    private $TipoProductoDAO;
    
    
    

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

 
    /**
     * @return tipoProductoDAO
     */
    public function getTipoProductoDAO()
    {
        return $this->tipoProductoDAO;
    }

    public function tipoProducto($id="", $nombre=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> Conexion = new conexion();
        $this -> TipoProductoDAO = new tipoProductoDAO($id, $nombre);
    }
    
    public function consultar(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> TipoProductoDAO -> consultar());
        $tiposProducto = array();
        while(($resultado = $this -> Conexion -> extraer()) != null){
            array_push($tiposProducto, new TipoProducto($resultado[0], $resultado[1]));
        }
        $this -> Conexion -> cerrar();
        return $tiposProducto;
        
        
    }
    
    
    
}
    
?>