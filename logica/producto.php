<?php
require_once 'persistencia/conexion.php';
require_once 'persistencia/productoDAO.php';
class producto{
    private $id;
    private $nombre;
    private $precio;
    private $cantidad;
    private $foto;
    private $Conexion;
    private $Administrador;
    private $tipoProducto;
    private $marca;
    
    
  
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Ambigous <string, unknown>
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @return mixed
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @return conexion
     */
    public function getConexion()
    {
        return $this->Conexion;
    }

    /**
     * @return mixed
     */
    public function getAdministrador()
    {
        return $this->Administrador;
    }

    /**
     * @return mixed
     */
    public function getTipoProducto()
    {
        return $this->tipoProducto;
    }

    /**
     * @return mixed
     */
    public function getMarca()
    {
        return $this->marca;
    }

    public function producto($id="", $nombre="", $precio="", $cantidad="", $foto="", $Administrador="", $marca="", $tipoProducto=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> cantidad = $cantidad;
        $this -> foto = $foto;
        $this -> Administrador = $Administrador;
        $this -> marca = $marca;
        $this -> tipoProducto = $tipoProducto;
        $this -> Conexion = new conexion();
        $this -> ProductoDAO = new productoDAO($id, $nombre, $precio, $cantidad, $foto, $Administrador, $marca, $tipoProducto);
    }
    
    public function crear(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ProductoDAO -> crear());
        $this -> Conexion -> cerrar();
    }
    
    
    public function consultarTodos($atributo, $direccion, $filas, $pag){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ProductoDAO -> consultarTodos($atributo, $direccion, $filas, $pag));
        $Productos = array();
        while(($resultado = $this -> Conexion -> extraer()) != null){
            $Administrador = new administrador($resultado[5]);
            $Administrador -> consultar();
            $Marca = new marca($resultado[6]);
            $Marca -> consultar();
            $Tipoproducto = new tipoProducto($resultado[7]);
            $Tipoproducto -> consultar();
            array_push($Productos, new producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $Administrador, $Marca, $Tipoproducto));
        }
        $this -> Conexion -> cerrar();
        return $Productos;
    }
    
    public function consultarTotalFilas(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ProductoDAO -> consultarTotalFilas());
        return $this -> Conexion -> extraer()[0];
    }
    
    
   /* public function consultar(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> AdministradorDAO -> consultar());
        $this -> Conexion -> cerrar();
        $datos = $this -> Conexion -> extraer();
        $this -> nombre = $datos[0];
        $this -> apellido = $datos[1];
        $this -> correo = $datos[2];
    }*/
    
}
