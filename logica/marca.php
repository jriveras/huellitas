<?php
require_once 'persistencia/conexion.php';
require_once 'persistencia/marcaDAO.php';
class marca{
    private $id;
    private $marca;
    private $Conexion;
    private $marcaDAO;
    
    
    

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMarca()
    {
        return $this->marca;
    }

 

    public function marca($id="", $marca=""){
        $this -> id = $id;
        $this ->marca = $marca;
        $this -> Conexion = new conexion();
        $this -> MarcaDAO = new marcaDAO($id, $marca);
    }
    
    public function consultar(){
        $this -> Conexion -> abrir();
       
        $this -> Conexion -> ejecutar($this -> MarcaDAO-> consultar() );
       
        $marcas = array();
        while(($resultado = $this -> Conexion -> extraer()) != null){
            array_push($marcas, new marca($resultado[0], $resultado[1]));
        }
        $this -> Conexion -> cerrar();
        return $marcas;
        
    }
    
    
    
}
    
?>