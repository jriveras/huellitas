<?php
require_once 'persistencia/conexion.php';
require_once 'persistencia/clienteDAO.php';
class cliente{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $Conexion;
    private $telefono;
    private $direccion;
    private $ClienteDAO;
    
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Ambigous <string, unknown>
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return Ambigous <string, unknown>
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * @return Ambigous <string, unknown>
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

  
   
    
    
    public function Administrador($id="", $nombre="", $apellido="", $correo="", $clave="", $telefono="", $direccion=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this ->telefono=$telefono;
        $this -> direccion = $direccion;
        $this -> Conexion = new conexion();
        $this -> ClienteDAO = new clienteDAO($this -> id, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this ->telefono,$this -> direccion );
    }
    
    public function autenticar(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> autenticar());
        $this -> Conexion -> cerrar();
        if($this -> Conexion -> numFilas() == 1){
            $this -> id = $this -> Conexion -> extraer()[0];
            return true;
        }else{
            return false;
        }
    }
    
    
    public function crear(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> crear());
        $this -> Conexion -> ejecutar($this -> ClienteDAO -> consultarUltimoId());
        $resultado = $this -> Conexion -> extraer();
        $this -> Conexion -> cerrar();
        return $resultado[0];
    }
    
    
    /*public function consultar(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> AdministradorDAO -> consultar());
        $this -> Conexion -> cerrar();
        $datos = $this -> Conexion -> extraer();
        $this -> nombre = $datos[0];
        $this -> apellido = $datos[1];
        $this -> correo = $datos[2];
    }
    
    
    public function consultarTodos(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> AdministradorDAO -> ConsultarTodos());
        $administradores = array();
        while(($resultado = $this -> Conexion -> extraer()) != null){
            array_push($administradores, new administrador($resultado[0], $resultado[1], $resultado[2], $resultado[3], ""));
        
        }
        $this -> Conexion -> cerrar();
        return  $administradores;
    }*/
    
    
}

?>