<?php

class productoDAO
{

    private $id;

    private $nombre;

    private $precio;

    private $cantidad;

    private $foto;

    private $administrador;

    private $marca;

    private $tipoProducto;

    public function ProductoDAO($id="", $nombre="", $precio="", $cantidad="", $foto="", $administrador="", $marca="", $tipoProducto=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> cantidad = $cantidad;
        $this -> foto = $foto;
        $this -> administrador = $administrador;
        $this -> marca = $marca;
        $this -> tipoProducto = $tipoProducto;
    }
    
    public function crear(){
        return "insert into producto (idProducto, nombre, precio, cantidad, Administrador_idAdministrador, Marca_idMarca, Tipoproducto_idTipoproducto)
                values (
                '" . $this -> id . "',
                '" . $this -> nombre . "',
                '" . $this -> precio . "',
                '" . $this -> cantidad . "',
                '" . $this -> administrador . "',
                '" . $this -> marca . "',
                '" . $this -> tipoProducto . "'
                )";
    }
    
    
    public function consultarTodos($atributo, $direccion, $filas, $pag){
        return "select idProducto, nombre, precio, cantidad, foto, Administrador_idAdministrador, Marca_idMarca, Tipoproducto_idTipoproducto
                from producto " .
                (($atributo != "" && $direccion != "")?"order by " . $atributo . " " . $direccion:"") .
                " limit " . (($pag-1)*$filas) . ", " . $filas;
    }
    
    public function consultarTotalFilas(){
        return "select count(idProducto)
                from producto";
    }
    
    
    
    
}
?>