
<?php


session_start();
require_once 'logica/administrador.php'; 
require_once "logica/marca.php";
require_once 'logica/tipoProducto.php';
require_once 'logica/producto.php';
require_once 'logica/cliente.php';


if(isset($_GET["sesion"]) && $_GET["sesion"] == "false"){
    $_SESSION["id"] = "";
    $_SESSION["rol"] = "";
}

$pid = "";
if (isset($_GET["pid"])) {
    $pid = base64_decode($_GET["pid"]);
}

$paginasSinSesion = array(
    "presentacion/inicio.php",
    "presentacion/autenticar.php",
    "presentacion/cliente/registro.php"
)
?>
<html>
<head>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
<link rel="icon" type="image/png" href="imagenes/imagencon.png"/>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>

</head>

<?php

if ($pid != "") {
    
    if(in_array($pid, $paginasSinSesion)){
        include $pid;
    }else{
        if(isset($_SESSION["id"]) && $_SESSION["id"] != ""){
            include $pid;
        }else{
            include 'presentacion/inicio.php';
        }
    }
    
    
} else {
    include 'presentacion/muestraArticulos.php';
}
?>