<?php
include 'presentacion/menuAdministrador.php';
$Administrador = new administrador();
$Administradores = $Administrador -> consultarTodos(); 
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Administrador</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>								
								<th scope="col">Id</th>
								<th scope="col">Nombre</th>
								<th scope="col">Apellido</th>
								<th scope="col">Correo</th>
								<th scope="col">Estado</th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							foreach ($Administradores as $administradorActual){
							    echo "<tr>";
							    echo "<td>" . $administradorActual -> getId() . "</td>";
							    echo "<td>" . $administradorActual -> getNombre() . "</td>";
							    echo "<td>" . $administradorActual -> getApellido() . "</td>";
							    echo "<td>" . $administradorActual -> getCorreo() . "</td>";
							    /*echo "<td>" . (($administradorActual -> getEstado()==1)?"<i id='iconoHabilitar" . $administradorActual -> getId() . "' class='fas fa-check-circle' data-bs-toggle='tooltip' data-bs-placement='bottom' title='Habilitado'></i>":"<i id='iconoHabilitar" . $administradorActual -> getId() . "' class='fas fa-times-circle' data-bs-toggle='tooltip' data-bs-placement='bottom' title='Deshabilitado'></i>") . "</td>";
							    if($_SESSION["id"] != $administradorActual -> getId()){
							        echo "<td><a href='#'>" . (($administradorActual -> getEstado()==1)?"<i id='botonHabilitar" . $administradorActual -> getId() . "' class='fas fa-user-times' data-bs-toggle='tooltip' data-bs-placement='bottom' title='Deshabilitar'></i>":"<i id='botonHabilitar" . $administradorActual -> getId() . "' class='fas fa-user-check' data-bs-toggle='tooltip' data-bs-placement='bottom' title='Habilitar'></i>") . "</a></td>";							        
							    }else{
							        echo "<td></td>";
							    }
							    echo "</tr>";	*/						    
							}
							?>
						</tbody>
					</table>
					<div id="resultados"></div>
				</div>
			</div>
		</div>
	</div>
</div>

