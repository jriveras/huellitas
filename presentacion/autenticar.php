<?php
$correo = $_POST["Correo"];
$clave = $_POST["Clave"];

$Administrador = new administrador("", "", "", $correo, $clave);
if ($Administrador->Autenticar()) {

    $_SESSION["id"] = $Administrador->getId();
    $_SESSION["rol"] = "Administrador";

    header("Location: index.php?pid=" . base64_encode("presentacion/sesionAdministrador.php"));
} else {

    header("Location:index.php?pid=" . base64_encode("presentacion/inicio.php"). "&error=1");
    
}

?>