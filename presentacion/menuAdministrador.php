<?php
$Administrador = new administrador($_SESSION["id"]);
$Administrador->consultar();
?>


	<div class="container-fluid" class="navbar " style="background-color: #e3f2fd;">
	
			<nav class="navbar navbar-expand-lg navbar-light container ">
				
					<a class="navbar-brand"
						href="Index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php") ?>"><i
						class="fas fa-home"></i></a>
					<button class="navbar-toggler" type="button"
						data-bs-toggle="collapse" data-bs-target="#navbarNav"
						aria-controls="navbarNav" aria-expanded="false"
						aria-label="Toggle navigation">
						<span class="navbar-toggler-icon" ></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">
						<ul class="navbar-nav ">
						
						<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
								href="#" id="navbarDropdown" role="button"
								data-bs-toggle="dropdown" aria-expanded="false">Administrador</a>
								<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
									<li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarAdministrador.php") ?>">Consultar Admin</a></li>
										<li><a class="dropdown-item" href="#">Consultar Clientes</a></li>
								</ul></li>
						
						
							<li class="nav-item"></li>
							<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
								href="#" id="navbarDropdown" role="button"
								data-bs-toggle="dropdown" aria-expanded="false"> Productos </a>
								<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
									<li><a class="dropdown-item"
										href="Index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">Crear</a></li>
									<li><a class="dropdown-item" href="Index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">Consultar</a></li>
									<li><hr class="dropdown-divider"></li>
									<li><a class="dropdown-item" href="#">Buscar</a></li>
								</ul></li>
						</ul>

						<ul class="navbar-nav ms-auto">
							<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
								href="#" id="navbarDropdown" role="button"
								data-bs-toggle="dropdown" aria-expanded="false">Administrador:
                						<?php echo $Administrador -> getNombre() . " " . $Administrador -> getApellido() ?></a>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="#">Editar Perfil</a> <a
										class="dropdown-item" href="#">Cambiar Clave</a>
								</div></li>
							<li class="nav-item"><a class="nav-link"
								href="index.php?sesion=false">Cerrar Sesion</a></li>
						</ul>

					</div>
			
			</nav>
				</div>
	
