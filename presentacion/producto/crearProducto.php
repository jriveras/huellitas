<?php
include 'presentacion/menuAdministrador.php';
if (isset($_POST["crear"])) {
    $Producto = new producto($_POST["IdProducto"], $_POST["nombre"], $_POST["precio"], $_POST["cantidad"], "", $_SESSION["id"], $_POST["marca"], $_POST["tipoProducto"]);
    $Producto->crear();
}

?>

<div class="container">
	<div class="row mt-3">
		<div class="col-sm-0 col-md-3"></div>
		<div class="col-sm-12 col-md-6">
			<div class="card">
				<h5 class="card-header">Crear Producto</h5>

				<div class="card-body text-center">


					<?php if (isset($_POST["crear"])) { ?>
					<div class="alert alert-success alert-dismissible fade show"
						role="alert">
						Datos ingresados correctamente.
						<button type="button" class="btn-close" data-bs-dismiss="alert"
							aria-label="Close"></button>
					</div>
					<?php } ?>



					<form
						action="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>"
						method="post">


						<div class="mb-3">
							<label class="form-label">Id Producto</label> <input
								type="number" class="form-control" name="IdProducto">

						</div>

						<div class="mb-3">
							<label class="form-label">Nombre</label> <input type="text"
								class="form-control" name="nombre">

						</div>

						<div class="mb-3">
							<label class="form-label">Precio</label> <input type="number"
								class="form-control" name="precio">

						</div>

						<div class="mb-3">
							<label class="form-label">Cantidad</label> <input type="number"
								class="form-control" name="cantidad">

						</div>

						<div class="mb-3">
							<label class="form-label">Marca</label> <select
								class="form-select" name="marca">
							<?php
    $Marca = new marca();
    $Marcas = $Marca->consultar();
    foreach ($Marcas as $marcaActual) {
        echo "<option value='" . $marcaActual->getId() . "'>" . $marcaActual->getMarca() . "</option>";
    }
    ?>							
							</select>
						</div>

						<div class="mb-3">
							<label class="form-label">Tipo Producto</label> <select
								class="form-select" name="tipoProducto">
							<?php
    $TipoProducto = new tipoProducto();
    $TipoProductos = $TipoProducto->consultar();
    foreach ($TipoProductos as $TipoProductoActual) {
        echo "<option value='" . $TipoProductoActual->getId() . "'>" . $TipoProductoActual->getNombre() . "</option>";
    }
    ?>							
							</select>
						</div>



						<button type="submit" class="btn btn-primary " name="crear">Crear</button>




					</form>


				</div>

			</div>
		</div>
	</div>
</div>